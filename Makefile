# Configurer le système 

# Move the files before executing them. Because if stored in a FAT32 flash
# drive, rights wont allow you to execute anything directly on the flash drive

TMP := ${shell mktemp -d}

all: install

install:
	cp -r . $(TMP)
	chmod a+x $(TMP)/configurerSysteme
	chmod a+x $(TMP)/installPackages
	chmod a+x $(TMP)/ajouterScripts
	chmod a+x $(TMP)/configurerBash
	chmod a+x $(TMP)/generateurMdp
	chmod a+x $(TMP)/configurerDwm
	chmod a+x $(TMP)/configurerEmacs
	chmod a+x $(TMP)/configurerGit
	chmod a+x $(TMP)/configurerDunst
	$(TMP)/configurerSysteme
	rm -rf $(TMP)

