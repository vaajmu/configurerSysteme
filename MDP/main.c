# include <stdio.h>
# include <stdlib.h>
# include <string.h>

// no comments
void afficherTable();
void afficherCoefs();
char *getMDP(int n);
char getChar(int colonne, int ligne);
int getLigne(int ligne);
int valide(char *mdp, int taille);
int veriftab(char *mdp, const char *tab);

// déclaration des tableaux de caractères
const char tabmin[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '\0'};

const char tabmaj[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '\0'};

const char tabnum[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '\0'};

const char tabsym[] = {'@', '!', ',', '$', '*', '(', ')', '=', '\0'};

const char *table[] = { tabmin, tabmaj, tabnum, tabsym };
// Nombre de caractères dans les tableaux
int ref[] = {26, 26, 10, 8};
// Coefficients de présence de chaque tableaux
int coefs[] = {0, 0, 0, 0};


int main (int argc, char** argv){
  srand(time(NULL));
  int nbChar, nbMDP, i, valider;
  char *mdp;

  // Test sur le nombre d'arguments
  if(argc != 8){
    printf("err args\n");
    printf("%s NBMDP NBCHAR a A 1 s valider?\n", argv[0]);
    return 1;
  }

  // Interprétation des arguments
  nbMDP = atoi(argv[1]);
  nbChar = atoi(argv[2]);  

  coefs[0] = atoi(argv[3]);
  coefs[1] = atoi(argv[4]) + coefs[0];
  coefs[2] = atoi(argv[5]) + coefs[1];
  coefs[3] = atoi(argv[6]) + coefs[2];

  valider = atoi(argv[7]);

  // Génération de nbMDP mots de passe
  for(i=0;i<nbMDP;i++){
    // Si valider vaut vrai, le mdp sera valide si
    // les tests sont passés
    if(valider){
      do{
	mdp = getMDP(nbChar);
      }while(!valide(mdp, nbChar));
    }
    // Sinon il n'y a pas de vérification
    else{
      mdp = getMDP(nbChar);
    }
    printf("%s\n", mdp);
  }

  return 0;
}

// Retourne 1 si un caractère de tab est présent dans mdp
int veriftab(char *mdp, const char *tab){
  if(strpbrk(mdp, tab) == NULL){
    return 0;
  }else{
    return 1;
  }
}

// Retourne 1 si un caractère de chaque
// tableau souhaité est présent
// Le test sur un tableau est effectué seulement
// si le coefficient est sup à 0
int valide(char *mdp, int taille){
  if(coefs[0] > 0){
    if(!veriftab(mdp, tabmin)){
      return 0;
    }
  }  
  if(coefs[1] > coefs[0]){
    if(!veriftab(mdp, tabmaj)){
      return 0;
    }
  }
  if(coefs[2] > coefs[1]){
    if(!veriftab(mdp, tabnum)){
      return 0;
    }
  }
  if(coefs[3] > coefs[2]){
    if(!veriftab(mdp, tabsym)){
      return 0;
    }
  }
  return 1;
}

// Fonction de test
void afficherTable(){
  int i, j;

  printf("Table : \n\n");

  printf("\t\tnb lignes : %d\n", *ref);

  for(i=0; i<4; i++){
    for(j=0; j<ref[i]; j++){
      printf("%c ", table[i][j]);
    }
    printf("\n");
  }

  printf("\n------------------------------\n");

}

// Fonction de test
void afficherCoefs(){
  int i;

  printf("Coefficients :\n\n\t");
  
  for(i=0; i<4; i++){
    printf("%d ", coefs[i]);
  }
  printf("\n--------------------------------\n");

}

// Retourne un mdp de n caractères
char *getMDP(int n){
  char *mdp = (char *)malloc(sizeof(char)*n);
  int i;
  
  for(i=0;i<n;i++){
    mdp[i] = getChar(rand(), rand());
  }
  
  return mdp;
}

// Retourne le caractère n° colonne du tableau
// n° ligne
char getChar(int colonne, int ligne){
  // Mise en forme du nombre ligne
  ligne = getLigne(ligne);
  // Mise en forme du nombre colonne
  colonne %= ref[ligne];

  return table[ligne][colonne];
}

// Retourne une ligne en prenant
// en compte les coeffs
int getLigne(int ligne){
  int i;
  ligne = ligne % coefs[3];
  for(i=0; i<4; i++){
    if(ligne < coefs[i])
      return i;
  }
  return 3;
}

