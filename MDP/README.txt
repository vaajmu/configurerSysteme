
"make all" compile le code et génère l'executable "mdp" dans le dossier 
      courrant

Générer des mots de passe :
	Sans arguments le programme renvoie une erreur et un pense-bête.
	./mdp NBMDP NBCHAR a A 1 s valider?

	NBMDP est le nombre de mots de passe à générer
	NBCHAR est le nombre de caractères par mots de passe
	a est le coefficient pour les minuscules
	A est le coefficient pour les majuscules
	1 est le coefficient pour les nombres
	s est le coefficient pour les symboles
	valider? vérifie, pour chaque coefficient superieur à 0, qu'un
		 de ses représentant est dans le mot de passe

