#! /bin/bash
# long-run.sh add time before calling commands and print an alert after used
# with aliases for commands that usually takes time, to be notified when it's
# over

# Time in seconds. If the commands run more than this time, the notification
# must be acknowledge.
ACK_THRESHOLD=10

startTime=$(date +%s)

# Time is a bash command, `time time command` will fail and is not what we
# want. We actually want `time command`
[[ "$1" = "time" ]] && shift

# Execute command
time "$@"
commandStatus=$?

stopTime=$(date +%s)

duration=$((stopTime - startTime))

# Urgency
urgency=low
[[ ${duration} -ge ${ACK_THRESHOLD} ]] && urgency=normal
[[ ${commandStatus} -ne 0 ]] && urgency=critical

# Timeout
timeout=5000
[[ ${commandStatus} -ne 0 ]] && timeout=0
[[ ${duration} -ge ${ACK_THRESHOLD} ]] && timeout=0

command="$1"

# Send notification. If command fails (arguments are incorrect), send an error
# notification
notify-send -t ${timeout} -u ${urgency} "${command} finished" "... after ${duration} seconds"
[[ $? -ne 0 ]] && notify-send -t 5000 -u critical "Error sending notification !"

exit ${commandStatus}
