#! /bin/bash
# chroot_notification_receiver.sh

ACK_THRESHOLD=10

while [[ 1 -eq 1 ]]
do
    msg=$(netcat -l -p 4800)
    read duration commandStatus command <<< "${msg}"
    # echo "Logging ${msg} - ${time} - ${command}"

    # Urgency
    urgency=low
    [[ ${duration} -ge ${ACK_THRESHOLD} ]] && urgency=normal
    [[ ${commandStatus} -ne 0 ]] && urgency=critical

    # Timeout
    timeout=5000
    [[ ${commandStatus} -ne 0 ]] && timeout=0
    [[ ${duration} -ge ${ACK_THRESHOLD} ]] && timeout=0

    # Send notification. If command fails (arguments are incorrect), send an error
    # notification
    notify-send -t ${timeout} -u ${urgency} "${command} finished" "... after ${duration} seconds"
    [[ $? -ne 0 ]] && notify-send -t 5000 -u critical "Error sending notification !"

    # notify-send $msg
done

exit 0
